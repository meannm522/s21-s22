//[SECTION] DATA MODELLING

//1. Identify what information we want to gather from the customers in order to determine wehter the user's identity is true

//=> WHY?
//1. to properly plan out what information will be deemed useful
//2. to lessen the chances or scenarios of having to modify or edit the data stored in the database.
//3.to anticipate how this data or information would relate to each other.

//TASK: Create a course Booking System for an INstitution

	//What are the minimum information would I need to collect from the customers/students

	//the following information described below would identify the structure of the user of the app
	// 1. first name
	// 2. last name
	// 3. middle name
	// 4. email address
	// 5. PIN/password
	// 6. Mobile number
	// 7. birthdate
	// 8. gender
	// 9. isAdmin: role and restriction/limitations that this buser ould have in our app.
	//10. dateTimeRegistered => when the student signed up/enrolled in the institution

//as a full stack web developer (front, backend, database)
//keep in mind, the more information you gather the more difficult it is to scale and managed the collection.

// Course/Subject
	// 1. name/title
	// 2. course code
	// 3. course description
	// 4. course unit
	// 5. course instructor
	//6. isActive => to describe if the course is being offered by the 		institution
	//7. dateTime created
	//8. courseSlots 

//2. Create an ERD to represent the enteties inside the database as well as to describe the relationship amongst them

	//users can have multiple subjects
	//subjects can have multiple enrollees

	//user can have multiple transaction
	//transaction can only belong to a single user


	//a single transaction can have multiple courses
	//a course can be part of multiple transaction


//3. convert and translate the ERD into a JSON like syntax in order to describe the structure of the document inside the collection by creating a mock data

//Note: creating/using MOCK data can play an integral role during the trsting phase of any app development.

//to generate mock data 
// https://www.mockaroo.com/ 